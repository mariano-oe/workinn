![WP, WP-Cli, Composer, Foundation, Sass and Grunt ](hero.png)

# WordPress Starting base template
This is the repository integrating some other repos about adding composer to wp and integrating foundation to a wp theme with the idea of simplify the starting process.

##We are using the following repos with the real hard work:
for composer install
https://github.com/johnpbloch/wordpress-core-installer

for the foundation theme, the amazing work of Ole Fredrik Lie.
we are using the code instead of forking to have our own configuration but I will recommend to take the latest version from the Ole's repo.
https://github.com/olefredrik/foundationpress


## Prerequisites
You will need WP-CLI installed in your box with a lamp (mysql, apache, php etc)

Installing cli in your system:
```
curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
```
Then, you need to make the file executable:
```
chmod +x wp-cli.phar
```

The final step is to move the file to a folder, so that you can execute it from anywhere. Renaming the file to something easy to remember and type is also a good idea; this is the reason why wp is the most commonly used name:
```
sudo mv wp-cli.phar /usr/local/bin/wp
wp --info
```

## Instructions to make the wordpress work in your dev box
### clone the master branch of this repo
```
git clone https://github.com/open-ecommerce/masters-wp.git
```
(or to the ssh address if you have a sshkey)

### navigate to the htdocs forder inside master-wp
```
cd masters-wp/htdocs
```

### get latest version of wp for your locale
(choose your locale from this site: http://wpcentral.io/internationalization/)
```
wp core download --locale=en_GB
```

### install wp for your db credentials
(change 'mynewwpdb' with the name of db you want create in the mysql server and your own credentials)
```
wp core config --dbname=mynewwpdb --dbuser=root --dbpass=123 --dbhost=localhost --dbprefix=oe34_
```

### create the db based in the just created wp-config
```
wp db create
```

### install and configure wp
- change 'myurl.dev' with the url you want to use localy.
- title is just the title inside the wp config
- change the wp admin credentials
- check more options at WP-CLI documentation: http://wp-cli.org/commands/core/install/
```
wp core install --url=myurl.dev  --title="Open-ecommerce wp master" --admin_user=oeadmin --admin_password=Password123
--admin_email="info@open-ecommerce.org"
```

### install the plugins via composer
- you can edit the composer.json file to add the pugins you want to install
- the wp plugins can be find at: http://wpackagist.org/
```
composer install
```

### add the domain to your dev box
- if you are in ubuntu you can use the file manager to do that: `sudo nautilus`
- copy the /docs/myurl.dev.conf and change the domain
- `sudo a2ensite myurl.dev.conf`
- add the domain to your /etc/hosts file
